function TestLiquidChannel() {
  camera.position.y = 2;
  camera.position.z = 3;
  var bd = new b2BodyDef;
  var ground = world.CreateBody(bd);

  var shape = new b2ChainShape;
  shape.vertices.push(new b2Vec2(-2, 0));
  shape.vertices.push(new b2Vec2(2, 0));
  shape.vertices.push(new b2Vec2(2, 4));
  shape.vertices.push(new b2Vec2(-2, 4));
  shape.CreateLoop();
  ground.CreateFixtureFromShape(shape, 0.0);

  var psd = new b2ParticleSystemDef();
  psd.radius = 0.007;
  var particleSystem = world.CreateParticleSystem(psd);

  shape = new b2PolygonShape;
  shape.SetAsBoxXYCenterAngle(2, 0.4, new b2Vec2(0, 3.6), 0);
  var pd = new b2ParticleGroupDef;
  pd.flags = b2_tensileParticle | b2_viscousParticle;
  pd.shape = shape;
  particleSystem.CreateParticleGroup(pd);


  bd = new b2BodyDef;
  var body = world.CreateBody(bd);
  shape = new b2EdgeShape;
  shape.Set(new b2Vec2(-2, 3.2), new b2Vec2(-1.2, 3.2));
  body.CreateFixtureFromShape(shape, 0.1);

  bd = new b2BodyDef;
  body = world.CreateBody(bd);
  shape = new b2EdgeShape;
  shape.Set(new b2Vec2(-1.1, 3.2), new b2Vec2(2, 3.2));
  body.CreateFixtureFromShape(shape, 0.1);

  var shape = new b2ChainShape;
  shape.vertices.push(new b2Vec2(-1.2, 3.2));
  shape.vertices.push(new b2Vec2(-1.2, 2.7));
  shape.vertices.push(new b2Vec2(.7, 2.7));
  shape.vertices.push(new b2Vec2(.7, 2.4));
  shape.vertices.push(new b2Vec2(-1.2, 2.4));
  shape.vertices.push(new b2Vec2(-1.2, 1.9));
  shape.vertices.push(new b2Vec2(.7, 1.9));
  shape.vertices.push(new b2Vec2(.7, 1.6));
  shape.vertices.push(new b2Vec2(-1.2, 1.6));
  shape.vertices.push(new b2Vec2(-1.2, 1.1));
  shape.vertices.push(new b2Vec2(1.1, 1.1));
  shape.vertices.push(new b2Vec2(1.1, 2.7));
  shape.vertices.push(new b2Vec2(1.3, 2.7));
  shape.vertices.push(new b2Vec2(1.3, 2));
  ground.CreateFixtureFromShape(shape, 0.0);

  var shape = new b2ChainShape;
  shape.vertices.push(new b2Vec2(-1.1, 3.2));
  shape.vertices.push(new b2Vec2(-1.1, 2.8));
  shape.vertices.push(new b2Vec2(.8, 2.8));
  shape.vertices.push(new b2Vec2(.8, 2.3));
  shape.vertices.push(new b2Vec2(-1.1, 2.3));
  shape.vertices.push(new b2Vec2(-1.1, 2.0));
  shape.vertices.push(new b2Vec2(.8, 2.0));
  shape.vertices.push(new b2Vec2(.8, 1.5));
  shape.vertices.push(new b2Vec2(-1.1, 1.5));
  shape.vertices.push(new b2Vec2(-1.1, 1.2));
  shape.vertices.push(new b2Vec2(1, 1.2));
  shape.vertices.push(new b2Vec2(1, 2.8));
  shape.vertices.push(new b2Vec2(1.4, 2.8));
  shape.vertices.push(new b2Vec2(1.4, 2));
  ground.CreateFixtureFromShape(shape, 0.0);


}